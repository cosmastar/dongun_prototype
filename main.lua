-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
-- DONGUNN v0.1.7.3
-- 13.10.2017
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
_centerX = display.contentCenterX
_centerY = display.contentCenterY
_contentWidth = display.actualContentWidth
_contentHeigth = display.actualContentHeight
-----------------------------------------------------------------------------------------
local composer = require("composer")
display.setStatusBar(display.HiddenStatusBar)
display.setDefault("background", 102 / 255, 204 / 255, 255 / 255 )

composer.gotoScene("src.scenes.game")

return scene