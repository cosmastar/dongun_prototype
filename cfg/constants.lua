local Constants = {

    time = {
        LONG_ATTACK = 1500,
        MIDDLE_ATTACK = 1000,
        FAST_ATTACK = 500,
        BLOCK = 250,
    },

    character = {
        hp = 3,
        -- speed = 0.3,
        distance = 100,
        -- distance = 50,
        speed = 1,
    },
    
    enemy = {
        hp = 3,
        -- speed = 0.3,
        distance = 50,
        -- distance = 150,
        speed = 1,
    },

    weapons = {
        throwDistance = 50,
    },

    stamina = {
        block = 0.25,
    },

    text = {
    	stateIdle = "idle",
    	stateAttack = "attack",
        statePowerfulAttack = "powerful attack",
        stateStartBlocking = "start blocking",
        stateFinishBlocking = "finish blocking",
        stateBlock = "block",

        health = "hp: ",
        attacktime = "attack: ",
        distance = "distance: ",
        distanceOK = "ok",
    	distanceBad = "bad",

        sucsessfullAttack = "Attack!",
        sucsessfullPowerfulAttack = "Powerful attack!",
    	sucsessfullBlock = "Attack has blocked!",
        sucsessfullCounterattack = "Counter-attack!",
    	unsucsessfullCounterattack = "Failed counter-attack!",
    },

};


return Constants