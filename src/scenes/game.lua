local composer = require( "composer" )
composer.recycleOnSceneChange = true

local cGame = require("src.classes.game")
local cCharacter = require("src.classes.character")
local cEnemy = require("src.classes.enemy")
local cLocation = require("src.classes.location")
local cInterface = require("src.classes.interface")
local cFightMaster = require("src.classes.fightMaster")
local utils = require("src.classes.utils")

local scene = composer.newScene()
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

    local parameters = event.params
    local sceneGroup = self.view

-----------------------------------------------------------------------------------
    Game = cGame:new()

    local location = cLocation:new()
    local interface = cInterface:new()

    local fightMaster = cFightMaster:new()
    local character = cCharacter:new(fightMaster)
    character:setPosition(_centerX - 120, _centerY)
    local enemy = cEnemy:new(fightMaster)
    enemy:setPosition(_centerX + 120, _centerY)

    fightMaster:setOpponents(character, enemy)

end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
        local prevScene = composer.getSceneName( "previous" )
        -- remove previous scene's view
        if (prevScene) then
            composer.removeScene( prevScene )
        end
    end

end


function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


function scene:destroy( event )

    local sceneGroup = self.view

end

-----------------------------------------------------------------------------------
-- Scene event function listeners
-----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-----------------------------------------------------------------------------------

return scene