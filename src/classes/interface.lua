-----------------------------------------------------------------------------------------
--[[  
Класс интерфейса.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Interface = Object:extend()
local Constants = require("cfg.constants")

function Interface:initialize(owner, ownerGroup)
    self.className = "Interface"
    self.owner = owner
    
    self.startBtn = {}
    self.startBtnText = {}
    self.objGroup = display.newGroup()

    self:createVisual()
    self:addTouchHandlers()
    self:setStopGameHandler()
    
end

function Interface:createVisual()
    self.startBtn = display.newRect( 0, 0, 95, 50 )
    -- self.startBtn.x
    self.startBtn.y = -100

    self.startBtn:setFillColor(250 / 255, 153 / 255, 58 / 255)
    self.objGroup:insert(self.startBtn)

    local options = 
    {
        text = "start game", 
        font = native.systemFont,
        fontSize = 16
    }

    self.startBtnText = display.newText( options )
    self.startBtnText.x = self.startBtn.x
    self.startBtnText.y = self.startBtn.y

    self.startBtnText:setFillColor( 1, 0, 0 )
    self.objGroup:insert(self.startBtnText)


    self.objGroup.x = _centerX
    self.objGroup.y = _centerY
    -- self.objGroup.alpha = 0

    -- self.objGroup.anchorChildren = true

    -- self:addInterfaceTouchHandlers()
end

function Interface:addTouchHandlers()
    -- print("Interface:addTouchHandlers")
    local this = self
    self.startBtn.touch = function(self, event)
        if ( event.phase == "began" ) then
            -- print("Interface startBtn touchHandler phase began.")
            -- Set touch focus
            display.getCurrentStage():setFocus( self )
            self.isFocus = true
        elseif ( self.isFocus ) then
            if ( event.phase == "moved" ) then
                -- print( "Moved phase of attack touch event detected." )
            elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
                -- Reset touch focus
                display.getCurrentStage():setFocus( nil )
                self.isFocus = nil

                Game:startGame()

                this.startBtn.alpha = 0
                this.startBtnText.alpha = 0

                -- print( "Ended or cancelled phase of interface startBtn touch event detected." )
            end
        end
        return true
    end
    self.startBtn:addEventListener("touch", self.startBtn )
end


function Interface:setStopGameHandler()
    local STOP_GAME_EVENT = { name="stop_game" }
    local stopGameHandler = function( event )
        -- print("stopGameHandler Interface")

    end
    Runtime:addEventListener( STOP_GAME_EVENT.name, stopGameHandler )
end

return Interface