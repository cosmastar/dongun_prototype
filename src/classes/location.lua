--[[  
Класс локации.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Location = Object:extend()
-- local physics = require("physics")

function Location:initialize()
    self.className = "Location"

    local floor = display.newRect( _centerX, _centerY + 55, _contentWidth, _contentHeigth )
    floor.anchorY = 0
    floor:setFillColor(0.5)

    -- physics.addBody( floor, "static", { density=1, friction=0.5, bounce=0 } )

end


return Location