-----------------------------------------------------------------------------------------
--[[  
Класс мастера дистанции.
Вспомогательный класс Мастера боя, управляет перемещением оппонентов.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local DistanceMaster = Object:extend()
local Constants = require("cfg.constants")
local utils = require("src.classes.utils")

-- local SPEED_LEFT_OPPONENT = 1
-- local SPEED_RIGHT_OPPONENT = 1
-- local SPEED_RIGHT_OPPONENT = 0.5

function DistanceMaster:initialize(opponents)
    self.className = "DistanceMaster"
    self.opponents = opponents

    self.update = {}

    self.btns = {}
    self.btnsTexts = {}
    self.statusRight = {}
    self.statusLeft = {}

    self.isLeftOK = false
    self.isRightOK = false

    --вспомогательные объекты для визуального представления границ оппонентов
    self.circ1 = display.newCircle( 0, 0, 2 )
    self.circ2 = display.newCircle( 0, 0, 2 )

    self:createVisual()
    self:setStartGameHandler()
    self:setStopGameHandler()
end

function DistanceMaster:setStartGameHandler()
    local START_GAME_EVENT = { name="start_game" }
    local startGameHandler = function( event )
        -- print("startGameHandler in DistanceMaster")
        self:approach()
    end
    Runtime:addEventListener( START_GAME_EVENT.name, startGameHandler )
end

function DistanceMaster:setStopGameHandler()
    local STOP_GAME_EVENT = { name="stop_game" }
    local stopGameHandler = function( event )
        -- print("stopGameHandler in DistanceMaster")
        self:stop()
    end
    Runtime:addEventListener( STOP_GAME_EVENT.name, stopGameHandler )
end

function DistanceMaster:createVisual()
    --визуальные статусы дистанций
    local options = 
    {
        text = Constants.text.distance .. Constants.text.distanceBad,
        font = native.systemFont,
        fontSize = 16,
    }
    self.statusLeft = display.newText( options )
    self.statusLeft.x = 50
    self.statusLeft.y = 50
    self.statusLeft:setFillColor( 1, 1, 0 )

    self.statusRight = display.newText( options )
    self.statusRight.x = display.contentWidth - 50
    self.statusRight.y = 50
    self.statusRight:setFillColor( 1, 1, 0 )

    --кнопки управления дистанцией
    -- self:addBtns()

end

function DistanceMaster:addBtns()
    self.btns[1] = display.newRect( 140, 250, 50, 35 )
    self.btns[1]:setFillColor(153 / 255, 51 / 255, 0 / 255)
    self.btns[2] = display.newRect( 340, 250, 50, 35 )
    self.btns[2]:setFillColor(153 / 255, 51 / 255, 0 / 255)

    local options = 
    {
        text = "back",
        font = native.systemFont,
        fontSize = 14,
    }
    self.btnsTexts[1] = display.newText( options )
    self.btnsTexts[1]:setFillColor( 1, 1, 0 )
    self.btnsTexts[1].x = self.btns[1].x
    self.btnsTexts[1].y = self.btns[1].y

    self.btnsTexts[2] = display.newText( options )
    self.btnsTexts[2]:setFillColor( 1, 1, 0 )
    self.btnsTexts[2].x = self.btns[2].x
    self.btnsTexts[2].y = self.btns[2].y
end

function DistanceMaster:approach()
    -- print("DistanceMaster:approach()")

    self.isLeftOK = false
    self.isRightOK = false
    self.opponents[1].isOnDistanceToHit = false
    self.opponents[2].isOnDistanceToHit = false

    local xFirst, xSecond
    local distanceBetweenOpponents

    self.update = function(event)
        -- print(self.opponents[1].objGroup.x)
        --X-координаты оппонентов
        -- local xFirst = self.opponents[1].objGroup.x + self.opponents[1].objGroup.width/2
        -- local xSecond = self.opponents[2].objGroup.x - self.opponents[1].objGroup.width/2

        xFirst = self.opponents[1].objGroup.x + self.opponents[1].obj.width
        xSecond = self.opponents[2].objGroup.x - self.opponents[1].obj.width
        --дистанция между ними
        distanceBetweenOpponents = xSecond - xFirst

        --вспомогательные объекты для визуального представления границ оппонентов
        self.circ1.x = xFirst
        self.circ1.y = self.opponents[1].objGroup.y
        self.circ2.x = xSecond
        self.circ2.y = self.opponents[2].objGroup.y

        --движение оппонента слева
        if distanceBetweenOpponents > self.opponents[1].distance then
            --временно отключаю движение левого оппонента!
            self.opponents[1].objGroup.x = self.opponents[1].objGroup.x + self.opponents[1].speed
            -- self.opponents[1].objGroup.x = self.opponents[1].objGroup.x
            --обновляю визуальный статус: слишком далеко для атаки 
            self.statusLeft.text = Constants.text.distance .. Constants.text.distanceBad
        else
            if not self.isLeftOK then
                --подошел на дистанцию атаки
                self.opponents[1].isOnDistanceToHit = true
                self.opponents[1].ui:unblockAttackInput()
            end
            --обновляю визуальный статус: подошел на расстояние атаки 
            self.isLeftOK = true
            self.statusLeft.text = Constants.text.distance .. Constants.text.distanceOK
        end

        --движение оппонента справа
        if distanceBetweenOpponents > self.opponents[2].distance then
            --временно отключаю движение правого оппонента!
            self.opponents[2].objGroup.x = self.opponents[2].objGroup.x - self.opponents[2].speed
            -- self.opponents[2].objGroup.x = self.opponents[2].objGroup.x
            --обновляю визуальный статус: слишком далеко для атаки 
            self.statusRight.text = Constants.text.distance .. Constants.text.distanceBad
        else
            if not self.isRightOK then
                --подошел на дистанцию атаки
                self.opponents[2].isOnDistanceToHit = true
                self.opponents[2].ui:unblockAttackInput()
            end
            --обновляю визуальный статус: подошел на расстояние атаки 
            self.isRightOK = true
            self.statusRight.text = Constants.text.distance .. Constants.text.distanceOK
        end

        if self.isLeftOK and self.isRightOK then
            self:stop()
        end

    end
    Runtime:addEventListener("enterFrame", self.update)
end

--[[
Метод вызывает отбрасывание игрока на некоторое расстояние назад после пропущенного
силового удара.
ПАРАМЕТР: <human> target - кто пропустил силовой удар.
ПАРАМЕТР: <human> iniciator - кто анес силовой удар.
]]
function DistanceMaster:throwBack(target, iniciator)
    target:throwBack(iniciator)
    self:stop()
    self:approach()
end

function DistanceMaster:stop()
    Runtime:removeEventListener("enterFrame", self.update)
end

return DistanceMaster