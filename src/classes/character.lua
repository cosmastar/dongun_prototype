-----------------------------------------------------------------------------------------
--[[  
Класс персонажа
2017
--]]
-----------------------------------------------------------------------------------------
local Human = require("src.classes.human")
local Character = Human:extend()
local HealthBar = require("src.classes.healthBar")
local Inventory = require("src.classes.inventory")
local Stamina = require("src.classes.stamina")
local cUI = require("src.classes.ui")
local Constants = require("cfg.constants")

function Character:initExt()
	self.className = "Character"
    self.state = Constants.text.stateIdle
    --тип состояния: id кнопки
    self.stateType = {}
    self.id = 1

	--Длина hitbox
    self.distance = Constants.character.distance
    self.isOnDistanceToHit = false
	self.speed = Constants.character.speed
    self.attackTime = Constants.time.MIDDLE_ATTACK

    self.healthPoints = Constants.character.hp
    self.currentHealthPoints = Constants.character.hp
    self.healthBar = HealthBar:new(self.healthPoints)
    self.stamina = Stamina:new(self)

    self:initView()
    self:initHitBox()

    self.inventory = Inventory:new(self)

    self.ui = cUI:new(self, self.id)

end

function Character:initView()
	--Объект
    self.obj = display.newRect( 0, 0, 50, 50 )
    self.obj:setFillColor(250 / 255, 153 / 255, 58 / 255)
    self.objGroup.anchorChildren = true
    self.objGroup.anchorX = 0
    self.objGroup.anchorY = 0
    self.objGroup:insert(self.obj)
end

function Character:initHitBox()
    local xHB = self.obj.x + self.obj.width/2
    local yHB = self.obj.y - self.obj.height/2
    self.hitBox = display.newRect( xHB, yHB, 50, 50 )
    self.hitBox:setFillColor(143 / 255, 64 / 255, 236 / 255)
    self.hitBox.anchorX = 0
    self.hitBox.anchorY = 0
    self.hitBox.alpha = 0.3

    --Длина hitbox
    self.hitBox.width = self.distance
    self.objGroup:insert(self.hitBox)
end

function Character:setPosition(x, y)
	self.objGroup.x = x
	self.objGroup.y = y

    self.inventory:setPosition(x + self.objGroup.width/2, y + self.objGroup.height/2)
    self.inventory:setStatusPosition(self.objGroup.x + self.obj.width/2, self.objGroup.y - 45)
    self.ui.statusBar:setPosition(self.objGroup.x + self.obj.width/2, self.objGroup.y - 25)
    self.ui.powerfulAtack:setPosition(self.objGroup.x + self.obj.width/2 + 25, self.objGroup.y + self.obj.height*2)
    self.healthBar:setPosition(self.objGroup.x + self.obj.width/2, self.objGroup.y - 10)
    self.stamina:setPosition(self.objGroup.x + self.obj.width/2, self.objGroup.y - 75)
end

--[[
Меняю состояние персонажа.
Возможные входные данные: <string> "idle", "attack", "block"
Пример: setState("idle")
]]
function Character:setState(state, stateType)
    -- print(":setState", state)
    self.state = state
    self.stateType = stateType
    -- print("self.stateType", self.stateType)
    self.fightMaster:handleAction(self.state, self.stateType, self)
end

--[[
Метод вызывает изменение счетчика здоровья.
]]
function Character:getDamage()
    self.currentHealthPoints = self.currentHealthPoints - 1
    self.obj.text = Constants.text.health .. self.currentHealthPoints
    self.healthBar:updateText(self.currentHealthPoints)
    -- self:setState(Constants.text.stateIdle, self.id)

    -- print(":getDamage()", self.currentHealthPoints)
    if self.currentHealthPoints == 0 then Game:stopGame(self.id) end
end


--[[
Метод вызывает отбрасывание игрока на некоторое расстояние назад после пропущенного
силового удара.
]]
function Character:throwBack(iniciator)
    -- print(":throwBack()")

    local xLeftBoundOpponent = self.objGroup.width + self.distance
    local leftBoundX = iniciator.objGroup.x - iniciator.objGroup.width
    -- local circ1 = display.newCircle( leftBoundX, iniciator.objGroup.y, 4 )

    self.objGroup.x = leftBoundX
    -- if self.objGroup.x > rightBoundX then
    --     self.objGroup.x = rightBoundX
    -- end

    self.objGroup.x = self.objGroup.x - Constants.weapons.throwDistance
    self.objGroup.y = self.objGroup.y
end

--[[
Меняю длительность атаки.
]]
function Character:setAttackTime(attackTime)
    -- print(":setState", state)
    self.attackTime = attackTime
end

function Character:getAttackTime()
    return self.attackTime
end

function Character:getState()
    return self.state
end

-- function Character:slowSpeed()
--     self.speed = 
-- end

return Character