-----------------------------------------------------------------------------------------
--[[  
Класс мощной атаки.
Атака считается мощной если игрок успевает надать на область тапа в отведенное время.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local PowerfulAtack = Object:extend()
local Constants = require("cfg.constants")

function PowerfulAtack:initialize(character)
    self.className = "PowerfulAtack"
    self.character = character
    
    self.obj = {}
    self.text = {}
    self.objGroup = display.newGroup()

    self.attackFinishedTimer = {}

    self:createVisual()

    self:setStartPointAttackHandler()
    
end

function PowerfulAtack:createVisual()

    self.obj = display.newRect( 0, 0, 100, 100 )
    self.obj:setFillColor( 1, 0, 0 )

    local options = 
    {
        text = "Attack!",
        x = self.obj.x,
        y = self.obj.y,
        font = native.systemFont,
        fontSize = 26
    }
    self.text = display.newText( options )
    self.text:setFillColor( 1 )

    self.objGroup:insert(self.obj)
    self.objGroup:insert(self.text)

    self:setAlpha(0)
    self:addTouchHandlers()

end

function PowerfulAtack:addTouchHandlers()
    local this = self
    self.objGroup.touch = function(self, event)
        if ( event.phase == "began" ) then
            -- print("powerful attack ontouchListener", self.id)
            -- Set touch focus
            -- display.getCurrentStage():setFocus( self )
            -- self.isFocus = true
            
            this:setAlpha(0)
            this.character:setState(Constants.text.statePowerfulAttack, nil)

        elseif ( self.isFocus ) then
            if ( event.phase == "moved" ) then
                -- print( "Moved phase of attack touch event detected." )
            elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
                -- Reset touch focus
                -- display.getCurrentStage():setFocus( nil )
                -- self.isFocus = nil
                -- this:setAlpha(0)
                -- this.character:setState(Constants.text.statePowerfulAttack, nil)
                -- print( "Ended or cancelled phase of powerful attack touch event detected." )
            end
        end
        return true
    end
    self.objGroup:addEventListener("touch", self.objGroup )
end

function PowerfulAtack:setAlpha(alpha)
    self.objGroup.alpha = alpha
end

function PowerfulAtack:setStartPointAttackHandler()
    local START_ATTACK_EVENT = { name="start_attack", iniciator = nil }
    local startPointAttackHandler = function( event )
        if event.iniciator == self.character.id then
            -- print("setStartPointAttackHandler", event.iniciator)

            --
            self.attackStartTimer = timer.performWithDelay(self.character:getAttackTime()/3, function()
                self:setAlpha(1)

                local timeHide = self.character:getAttackTime() - self.character:getAttackTime()/3
                self.attackFinishedTimer = timer.performWithDelay(timeHide, function()
                    self:setAlpha(0)
                end)
            end)
        end
    end
    Runtime:addEventListener( START_ATTACK_EVENT.name, startPointAttackHandler )
end

function PowerfulAtack:setPosition(x, y)
    self.objGroup.x = x
	self.objGroup.y = y
end

return PowerfulAtack