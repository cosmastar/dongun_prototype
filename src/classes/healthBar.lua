-----------------------------------------------------------------------------------------
--[[  
Класс хелф бара.
Над персонажем отображается статус его злоровья.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local HealthBar = Object:extend()
local Constants = require("cfg.constants")

function HealthBar:initialize(healthPoints)
    self.className = "HealthBar"
    self.initialHealthPoints = healthPoints
    -- self.currentHealthPoints = self.initialHealthPoints
    
    self.obj = {}
    self.objGroup = display.newGroup()

    self:createVisual()
    -- self:addUpdate()
    
end

function HealthBar:createVisual()
    local options = 
    {
        text = Constants.text.health .. self.initialHealthPoints, 
        font = native.systemFont,
        fontSize = 16
    }

    self.obj = display.newText( options )
    self.obj:setFillColor( 1, 0, 0 )
    self.objGroup:insert(self.obj)

end

function HealthBar:updateText(healthPoints)
    -- print("updateText")
    self.obj.text = Constants.text.health .. healthPoints
end

function HealthBar:setPosition(x, y)
    self.objGroup.x = x
	self.objGroup.y = y
end

return HealthBar