local tableUtils = {}

local Constants = require("cfg.constants")

function tableUtils.printTable ( t )
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

function tableUtils._setGradientBackground(_backgroundRect, sceneGroup)
    local paint = {
        type = "gradient",
        color1 = { 197/255, 232/255, 255/255},
        color2 = { 222/255, 239/255, 255/255 },
        direction = "down"
    }
    local rectWidth, rectHeight = _contentWidth - display.screenOriginX*2, _contentHeigth - display.screenOriginY*2
    local _backgroundRect = display.newRect( _centerX, _centerY, rectWidth, rectHeight )
    _backgroundRect.fill = paint
    sceneGroup:insert(_backgroundRect)
end

function tableUtils.createText()
    local options = 
    {
        text = Constants.text.sucsessfullBlock, 
        font = native.systemFont,
        fontSize = 16
    }

    local sucsessfullBlock = display.newText( options )
    sucsessfullBlock.alpha = 0
    sucsessfullBlock:setFillColor( 1, 0, 0 )

    sucsessfullBlock.x = _centerX
    sucsessfullBlock.y = _centerY - 60

    return sucsessfullBlock
end

-- local function garbagePrinting()
--     collectgarbage("collect")
--     local memUsage_str = string.format( "memUsage = %.3f KB", collectgarbage( "count" ) )
--     print( memUsage_str )
--     local texMemUsage_str = system.getInfo( "textureMemoryUsed" )
--     texMemUsage_str = texMemUsage_str/1000
--     texMemUsage_str = string.format( "texMemUsage = %.3f MB", texMemUsage_str )
--     print( texMemUsage_str )
-- end
-- -- Runtime:addEventListener( "enterFrame", garbagePrinting )

return tableUtils