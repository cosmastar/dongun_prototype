-----------------------------------------------------------------------------------------
--[[  
Класс стека. Реализован с помощью таблицы (массива).
Вспомогательный класс; используется Мастером боя в своей работе для хранения действий.
Примеры использования:

	local stack = cStack:new(2)
	stack:push(10)
	stack:push(20)

	stack:peek()
	print("peek stack item:", item)

	while(not stack:isEmpty()) do
	    local item = stack:pop()
	    print("stack item:", item)
	end

2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Stack = Object:extend()

--[[
Конструктор.
Параметр maxSize - максимальное число элементов в стеке.
]]
function Stack:initialize(maxSize)
    self.className = "Stack"
    
    self.stackArray = {}
    self.maxElements = maxSize	--максимальное число элементов
    self.top = 0	--вершина стека
end

--[[
Размещение элемента на вершине стек
]]
function Stack:push(item)
	if (not self:isFull()) then
		self.top = self.top + 1
		self.stackArray[self.top] = item
		-- print("Push item", item, " at", self.top)
		print("Push item", item.name, " at", self.top)
	else
		print("Can't push item", item.name, ": stack is full!")
	end
end

--[[
Извлечение элемента с вершины стека
]]
function Stack:pop()
	if (self.top > 0) then
		local item = self.stackArray[self.top]
		print("Pop item", item, "from index", self.top)
		self.top = self.top - 1
		return item
	else
		print("Can't pop item: stack is empty!")
	end
end

--[[
Чтение элемента с вершины стека
]]
function Stack:peek()
	if (self.top > 0) then
		return self.stackArray[self.top]
	else
		print("Can't peek item: stack is empty!")
	end
end

--[[
True, если стек пуст
]]
function Stack:isEmpty()
	return self.top == 0
end

--[[
True, если стек полон
]]
function Stack:isFull()
	return self.top == self.maxElements
end

return Stack