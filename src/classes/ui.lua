--[[  
Класс интерфейса.
Отвечает за пользовательский ввод (кнопки атак и блоков).
В основном делегирует ввод Мастеру боя.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local UI = Object:extend()
local Constants = require("cfg.constants")
local StatusBar = require("src.classes.statusBar")
local PowerfulAtack = require("src.classes.powerfulAtack")

local uicoordinates = {
	enemyX1 = 225,
	enemyX2 = 160,
	characterX1 = -225,
	characterX2 = -160,
}

function UI:initialize(character, mode)
    self.className = "UI"

    self.character = character
    self.mode = mode

    self.attackFinishedTimer = {}
    self.unblockInputTimer = {}
    self.startBlockingTimer = {}
    self.finishBlockingTimer = {}
    self.unblockInputTimerAfterBlock = {}
    self.statusBar = {}

    self.startBlockTime = {}
    self.endBlockTime = {}

    --функция-листенер нажатия клавиш блока оппонента справа
    self.keyboardListenerBlock = {}

    self:createVisual()

    self:setStartGameHandler()
    self:setStopGameHandler()
    self:setDisableActionHandler()
end

--[[
Создаю визуальную часть кнопки.
По id отличаю пары кнопок атаки/блока (id=1,2,3)
]]
function UI:createVisual()
    self.attackBtns = {}
    self.blockBtns = {}
    self.attackTexts = {}
    self.blockTexts = {}

    for i = 1, 3 do 
    	self.attackBtns[i] = display.newCircle( _centerX - 225, 100 + 70 * (i-1), 25 )
    	-- self.attackBtns[i] = display.newCircle( _centerX - 200, 100 + 70 * (i-1), 25 )
    	self.attackBtns[i].id = i

    	self.blockBtns[i] = display.newCircle( _centerX - 160, 100 + 70 * (i-1), 25 )
    	-- self.blockBtns[i] = display.newCircle( _centerX + 200, 100 + 70 * (i-1), 25 )
    	self.blockBtns[i].id = i
-----------------------------------------------------------------------------------------
-- временный хак, чтобы вручную управлять противником!
    	if self.mode == 1 then
    		self.attackBtns[i].x = _centerX - uicoordinates.enemyX1
			self.blockBtns[i].x = _centerX - uicoordinates.enemyX2
		else
    		self.attackBtns[i].x = _centerX - uicoordinates.characterX2
			self.blockBtns[i].x = _centerX - uicoordinates.characterX1
		end
-----------------------------------------------------------------------------------------
		--добавляю подписи кнопкам атаки
	    local options = 
	    {
	    	text = "A" .. i,
	    	x = self.attackBtns[i].x,
	    	y = self.attackBtns[i].y,
	        font = native.systemFont,
	        fontSize = 16
	    }
	    self.attackTexts[i] = display.newText( options )
	    self.attackTexts[i]:setFillColor( 1, 0, 0 )

	    --и кнопкам блокирования
	    options = 
	    {
	    	text = "B" .. i,
	    	x = self.blockBtns[i].x,
	    	y = self.blockBtns[i].y,
	        font = native.systemFont,
	        fontSize = 16
	    }
	    self.blockTexts[i] = display.newText( options )
	    self.blockTexts[i]:setFillColor( 1, 0, 0 )

    end

    self.powerfulAtack = PowerfulAtack:new(self.character)
    self.statusBar = StatusBar:new(self.character)

    self:addTouchHandlers()
end

function UI:addTouchHandlers()
	local this = self
    for i = 1, 3 do 
	    self.attackBtns[i].touch = function(self, event)
	    	if ( event.phase == "began" ) then
	    		if (this.character.isOnDistanceToHit) then
		        	-- print("attack ontouchListener", self.id)
	    	        -- Set touch focus
			        display.getCurrentStage():setFocus( self )
			        self.isFocus = true
			    end
	        elseif ( self.isFocus ) then
		        if ( event.phase == "moved" ) then
		            -- print( "Moved phase of attack touch event detected." )
		        elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
		            -- Reset touch focus
		            display.getCurrentStage():setFocus( nil )
		            self.isFocus = nil
		            this:blockAttackInput(true)
		            this:blockBlockInput()

		            -- print("this.character.isOnDistanceToHit", this.character.isOnDistanceToHit)

		            local START_ATTACK_EVENT = { name="start_attack", iniciator = this.character.id }
		            Runtime:dispatchEvent( START_ATTACK_EVENT )

		            this.character:setState(Constants.text.stateAttack, self.id)
		            -- print( "Ended or cancelled phase of attack touch event detected." )
		        end
	        end
	        return true
	    end
    	-- self.attackBtns[i]:addEventListener("touch", self.attackBtns[i] )
    	
	    self.blockBtns[i].touch = function(self, event)
	    	if ( event.phase == "began" ) then
	        	-- print("block ontouchListener", self.id)
    	        -- Set touch focus
		        display.getCurrentStage():setFocus( self )
		        self.isFocus = true

		        --запоминаю время нажатия кнопки блокирования
				this.startBlockTime = os.clock()
		        --состояние персонажа - "начинается блокирование"".
		        --это все еще не блок, это подготовка к нему.
		        this.character:setState(Constants.text.stateStartBlocking, self.id)
		        --запускаю таймер, по срабатыванию которого блок считается успешным
		        --(если кнопка не была отпущена)
				this.startBlockingTimer = timer.performWithDelay(Constants.time.BLOCK, 
					function()
						-- print("startBlockingTimer")
						this.character:setState(Constants.text.stateBlock, self.id)
					end)				

	        elseif ( self.isFocus ) then
		        if ( event.phase == "moved" ) then
		            -- print( "Moved phase of block touch event detected." )
		        elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
		            -- print( "Ended or cancelled phase of block touch event detected." )
		            -- Reset touch focus
		            display.getCurrentStage():setFocus( nil )
		            self.isFocus = nil

		            --если кнопка была отпущена, то не засчитывать блок (отменяю таймер, 
		            --по срабатыванию которого блок считается успешным).
	            	timer.cancel(this.startBlockingTimer)
	            	--запоминаю время отпускания кнопки блокирования
	            	this.endBlockTime = os.clock()
	            	--получаю время удерживания кнопки блокирования
	            	local timeHoldBlock = (this.endBlockTime - this.startBlockTime) * 1000
	            	-- print(timeHoldBlock, Constants.time.BLOCK)

	            	--если прошло недостаточно времени (разница превышает время из конфига)
	            	if timeHoldBlock <= Constants.time.BLOCK then
	            		--то заблокировать ввод на некоторое время
	            		local delta = Constants.time.BLOCK - timeHoldBlock
	            		-- print("need to block input")
	            		-- print("delta", delta)
	            		this.character:setState(Constants.text.stateFinishBlocking, self.id)

	            		this:blockAttackInput()
	            		this:blockBlockInput()
						this.unblockInputTimerAfterBlock = timer.performWithDelay(delta, 
							function()
								-- print("unblockInputTimerAfterBlock")
								this:unblockAttackInput()
								this:unblockBlockInput()
	            				this.character:setState(Constants.text.stateIdle, self.id)
							end)

	            		-- this.character:setState(Constants.text.stateIdle, self.id)
	            	else
	            		--иначе нужно блокировать ввод на базовое время блока
	            		-- print("do not block input")
	            		this.character:setState(Constants.text.stateFinishBlocking, self.id)

	            		this:blockAttackInput()
	            		this:blockBlockInput()
						this.finishBlockingTimer = timer.performWithDelay(Constants.time.BLOCK,
							function()
								this:unblockAttackInput()
								this:unblockBlockInput()
	            				this.character:setState(Constants.text.stateIdle, self.id)
							end)
	            		-- this.character:setState(Constants.text.stateIdle, self.id)
	            	end

		        end
	        end
	        return true
	    end
    	-- self.blockBtns[i]:addEventListener("touch", self.blockBtns[i] )
    end

	self:addKeyboardHandler()
end

function UI:blockAttackInput(isTimer)
	-- print("blockAttackInput")
	for i = 1, 3 do 
		self.attackBtns[i]:removeEventListener("touch", self.attackBtns[i] )
		-- self.blockBtns[i]:removeEventListener("touch", self.blockBtns[i] )
	end

	if isTimer then
		self.attackFinishedTimer = timer.performWithDelay(self.character:getAttackTime(), function()
			self:unblockAttackInput()
			self:unblockBlockInput()
			self.character:setState(Constants.text.stateIdle)
		end)
	end
end

function UI:unblockAttackInput()
	-- print("unblockAttackInput")
	for i = 1, 3 do 
		self.attackBtns[i]:addEventListener("touch", self.attackBtns[i] )
		-- self.blockBtns[i]:addEventListener("touch", self.blockBtns[i] )
	end
end

function UI:blockBlockInput()
	-- print("blockBlockInput")
	Runtime:removeEventListener( "key", self.keyboardListenerBlock )
	for i = 1, 3 do 
		self.blockBtns[i]:removeEventListener("touch", self.blockBtns[i] )
	end
end

function UI:unblockBlockInput()
	-- print("unblock input only block")
	Runtime:addEventListener( "key", self.keyboardListenerBlock )
	for i = 1, 3 do 
		self.blockBtns[i]:addEventListener("touch", self.blockBtns[i] )
	end
end

function UI:setStartGameHandler()
    local START_GAME_EVENT = { name="start_game" }
    local startGameHandler = function( event )
        -- print("setStartGameHandler", self.className)
        self:unblockBlockInput()
    end
    Runtime:addEventListener( START_GAME_EVENT.name, startGameHandler )
end

function UI:setStopGameHandler()
    local STOP_GAME_EVENT = { name="stop_game" }
    local stopGameHandler = function( event )
        -- print("stopGameHandler UI")
        self:blockAttackInput()
        self:blockBlockInput()
    end
    Runtime:addEventListener( STOP_GAME_EVENT.name, stopGameHandler )
end

function UI:setDisableActionHandler()
    --Отменяю текущее действие учсастников боя, перевожу их в состояние idle.
    local DISABLE_ACTIONS_EVENT = { name="disable_actions" }
    local disableActionsHandler = function( event )
        -- print("disableActionsHandler")
        self:cancelAction()
    end
    Runtime:addEventListener( DISABLE_ACTIONS_EVENT.name, disableActionsHandler )
end

function UI:cancelAction()
	timer.cancel(self.attackFinishedTimer)

	self.unblockInputTimer = timer.performWithDelay(150, function()
		self:unblockAttackInput()
		self.character:setState(Constants.text.stateIdle)
	end)
end

function UI:addKeyboardHandler()
	-- print("addKeyboardHandler")
	local this = self
	local BLOCK_ID
    --временная тестовая функция: блок правым противником можно поставить используя клавишу W
    -- local function keyboardListenerBlock( event )	 
    self.keyboardListenerBlock = function( event )
    	if ( event.phase == "down" ) then
	    	if ( (event.keyName == "w" or event.keyName == "s" or event.keyName == "x") 
	    		and self.character.id == 2 ) then
    		-- print( event.keyName .. " touched" )

	    	BLOCK_ID = this:defineBlockId(event.keyName)

	        --запоминаю время нажатия кнопки блокирования
			this.startBlockTime = os.clock()
	        --состояние персонажа - "начинается блокирование"".
	        --это все еще не блок, это подготовка к нему.
	        this.character:setState(Constants.text.stateStartBlocking, BLOCK_ID)
	        --запускаю таймер, по срабатыванию которого блок считается успешным
	        --(если кнопка не была отпущена)
			this.startBlockingTimer = timer.performWithDelay(Constants.time.BLOCK, 
				function()
					print("startBlockingTimer")
					this.character:setState(Constants.text.stateBlock, BLOCK_ID)
				end)	

	    	end
	    else
	    	if ( (event.keyName == "w" or event.keyName == "s" or event.keyName == "x")
	    			 and self.character.id == 2 ) then
	    		-- print( event.keyName .. " released")

	    		BLOCK_ID = this:defineBlockId(event.keyName)

				--если кнопка была отпущена, то не засчитывать блок (отменяю таймер, 
	            --по срабатыванию которого блок считается успешным).
            	timer.cancel(this.startBlockingTimer)
            	--запоминаю время отпускания кнопки блокирования
            	this.endBlockTime = os.clock()
            	--получаю время удерживания кнопки блокирования
            	local timeHoldBlock = (this.endBlockTime - this.startBlockTime) * 1000
            	-- print(timeHoldBlock, Constants.time.BLOCK)

            	--если прошло недостаточно времени (разница превышает время из конфига)
            	if timeHoldBlock <= Constants.time.BLOCK then
            		--то заблокировать ввод на некоторое время
            		local delta = Constants.time.BLOCK - timeHoldBlock
            		-- print("need to block input")
            		-- print("delta", delta)
            		this.character:setState(Constants.text.stateFinishBlocking, BLOCK_ID)

            		this:blockAttackInput()
            		this:blockBlockInput()
					this.unblockInputTimerAfterBlock = timer.performWithDelay(delta, 
						function()
							-- print("unblockInputTimerAfterBlock")
							this:unblockAttackInput()
							this:unblockBlockInput()
            				this.character:setState(Constants.text.stateIdle, self.id)
						end)

            		-- this.character:setState(Constants.text.stateIdle, self.id)
            	else
            		--иначе нужно блокировать ввод на базовое время блока
            		-- print("do not block input")
            		this.character:setState(Constants.text.stateFinishBlocking, BLOCK_ID)

            		this:blockAttackInput()
            		this:blockBlockInput()
					this.finishBlockingTimer = timer.performWithDelay(Constants.time.BLOCK,
						function()
							this:unblockAttackInput()
							this:unblockBlockInput()
            				this.character:setState(Constants.text.stateIdle, self.id)
						end)
            		-- this.character:setState(Constants.text.stateIdle, self.id)
            	end

	    	end
	    end
	    return false
	end
	-- Runtime:addEventListener( "key", self.keyboardListenerBlock )
end

--Определяю какой блок поставить на основании того какая клавиша была нажата
function UI:defineBlockId(keyName)
	if keyName == "w" then
		return 1
	elseif keyName == "s" then
		return 2
	elseif keyName == "x" then
		return 3
	end
end

return UI