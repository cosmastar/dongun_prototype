-----------------------------------------------------------------------------------------
--[[  
Класс игры.
Управляет состояниями игры.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Game = Object:extend()
local Constants = require("cfg.constants")

function Game:initialize()
    self.className = "Game"
    self.state = {}

    self.isGame = false
end

-- function Game:setState(state)
--     self.state = state
-- end

function Game:startGame()
    self.isGame = true

    local START_GAME_EVENT = { name="start_game" }
    Runtime:dispatchEvent( START_GAME_EVENT )
end

function Game:stopGame(id)
    -- print("stopGame, opponent with id=", id, "lose game")
    self.isGame = false

    local STOP_GAME_EVENT = { name="stop_game" }
    Runtime:dispatchEvent( STOP_GAME_EVENT )
end

return Game