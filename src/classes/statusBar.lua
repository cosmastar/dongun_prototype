-----------------------------------------------------------------------------------------
--[[  
Класс статус бара.
Над персонажем отображается его текущий статус: idle, attack, block.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local StatusBar = Object:extend()
local Constants = require("cfg.constants")

function StatusBar:initialize(character)
    self.className = "StatusBar"
    self.character = character
    
    self.obj = {}
    self.objGroup = display.newGroup()

    self:createVisual()
    self:addUpdate()
    
end

function StatusBar:createVisual()
    local options = 
    {
        text = Constants.text.stateIdle, 
        font = native.systemFont,
        fontSize = 16
    }

    self.obj = display.newText( options )
    self.obj:setFillColor( 1, 0, 0 )

    self.objGroup:insert(self.obj)

end

function StatusBar:addUpdate()
    self.update = function(event)
        self.obj.text = self.character:getState()
    end
    Runtime:addEventListener( "enterFrame", self.update )
    -- Runtime:removeEventListener( "enterFrame", self.update )

end

function StatusBar:setPosition(x, y)
    self.objGroup.x = x
	self.objGroup.y = y
end

return StatusBar