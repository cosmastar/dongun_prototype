-----------------------------------------------------------------------------------------
--[[  
Класс инвентаря.
Отображается при нажатии на персонажа. В инвентаре можно выбрать оружие.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Inventory = Object:extend()
local Constants = require("cfg.constants")

local attackTime = { }
attackTime[1] = Constants.time.FAST_ATTACK
attackTime[2] = Constants.time.MIDDLE_ATTACK
attackTime[3] = Constants.time.LONG_ATTACK

function Inventory:initialize(owner, ownerGroup)
    self.className = "Inventory"
    self.owner = owner
    
    -- self.obj = {}
    self.weapons = {}
    self.weaponsText = {}

    self.status = {}

    self.objGroup = display.newGroup()

    self:createVisual()
    self:addOwnerTouchHandler()
    
end

function Inventory:createVisual()
    local i
    for i=1,3 do
        self.weapons[i] = display.newRect( 0, 0, 50, 50 )
        self.weapons[i].y = (self.weapons[i].height+5) * (i - 1)

        self.weapons[i]:setFillColor(250 / 255, 153 / 255, 58 / 255)
        self.objGroup:insert(self.weapons[i])

        local options = 
        {
            text = attackTime[i], 
            font = native.systemFont,
            fontSize = 16
        }

        self.weaponsText.text = display.newText( options )
        self.weaponsText.text.x = self.weapons[i].x
        self.weaponsText.text.y = self.weapons[i].y

        self.weaponsText.text:setFillColor( 1, 0, 0 )
        self.objGroup:insert(self.weaponsText.text)

    end

    self.objGroup.alpha = 0

    self.objGroup.anchorChildren = true
    
    self:createInventoryStatus()
    self:addInventoryTouchHandlers()
end

function Inventory:createInventoryStatus()
    local options = 
    {
        text = Constants.text.attacktime .. self.owner:getAttackTime(), 
        font = native.systemFont,
        fontSize = 16,
    }

    self.status = display.newText( options )
    self.status:setFillColor( 1, 1, 0 )

    self.update = function(event)
        self.status.text = Constants.text.attacktime .. self.owner:getAttackTime()
    end
    Runtime:addEventListener( "enterFrame", self.update )

end

function Inventory:addOwnerTouchHandler()
    local this = self
    self.owner.objGroup.touch = function(self, event)
        if ( event.phase == "began" ) then
            -- print("inventoryTouchHandler")
            -- Set touch focus
            display.getCurrentStage():setFocus( self )
            self.isFocus = true
        elseif ( self.isFocus ) then
            if ( event.phase == "moved" ) then
                -- print( "Moved phase of attack touch event detected." )
            elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
                -- Reset touch focus
                display.getCurrentStage():setFocus( nil )
                self.isFocus = nil

                if this.objGroup.alpha == 0 then
                    this.objGroup.alpha = 1
                else
                    this.objGroup.alpha = 0
                end

                -- print( "Ended or cancelled phase of attack touch event detected." )
            end
        end
        return true
    end
    self.owner.objGroup:addEventListener("touch", self.owner.objGroup )
end

function Inventory:addInventoryTouchHandlers()
    local this = self

    local i
    for i=1,3 do

        self.weapons[i].touch = function(self, event)
            if ( event.phase == "began" ) then
                -- print("inventory item selected")
                -- Set touch focus
                display.getCurrentStage():setFocus( self )
                self.isFocus = true
            elseif ( self.isFocus ) then
                if ( event.phase == "moved" ) then
                    -- print( "Moved phase of attack touch event detected." )
                elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
                    -- Reset touch focus
                    display.getCurrentStage():setFocus( nil )
                    self.isFocus = nil

                    -- attackTime[i]
                    this.owner:setAttackTime(attackTime[i])
                    -- print( "Ended or cancelled phase of inventory item selected." )
                end
            end
            return true
        end
        self.weapons[i]:addEventListener("touch", self.weapons[i] )

    end

end

function Inventory:disableTouchHandlers()
    self.owner.objGroup:removeEventListener("touch", self.owner.objGroup )
end

function Inventory:setPosition(x, y)
    self.objGroup.x = x
    self.objGroup.y = y

    -- self.status.x = x
    -- self.status.y = self.owner.objGroup.y - 40
end

function Inventory:setStatusPosition(x, y)
    self.status.x = x
    self.status.y = y
end

return Inventory