-----------------------------------------------------------------------------------------
--[[  
Базовый класс человека
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Human = Object:extend()

function Human:initialize(fightMaster)
	self.className = "Human"
    -- self.lefthand = {}
    -- self.righthand = {}
    -- self.body = {}
    -- self.legs = {}
    -- self.behaviour = {}
    -- self.hp = {}
    -- self.armor = {}

    self.obj = {}
    self.objGroup = display.newGroup()

    self.hitBox = {}
    self.healthBar = {}
    self.inventory = {}

    self.healthPoints = {}

    self.distance = {}      --дальность атаки
    self.speed = {}      --скорость перемещения
    self.attackTime = {}    --длительность атаки

    self.state = {}
    self.fightMaster = fightMaster
    self.id = {}

    self.ui = {}

    self:initExt()
    
end

function Human:initExt()
    --@override
end

function Human:setPosition()
	--@override
end

function Human:updateHealthPoints()
    --@override
end

function Human:getAttackTime()
    --@override
    return self.attackTime
end

return Human