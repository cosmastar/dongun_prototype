-----------------------------------------------------------------------------------------
--[[  
Класс мастера боя.
Мастер боя управляет ходом боя. Определяет кому будет нанесен урон.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local FightMaster = Object:extend()
local Constants = require("cfg.constants")
local cStack = require("src.classes.stack")
local cDistanceMaster = require("src.classes.distanceMaster")
local utils = require("src.classes.utils")

function FightMaster:initialize()
    self.className = "FightMaster"
    
    self.stack = cStack:new(3)
    self.isStackAlive = false
    self.initTime = {}   --время инициализации стека
    self.stackInitiator = {}   --инициатор стека чтобы отследить конец жизни стека
    self.opponents = {} --оба участника боя
    self.other = {} --другой участник боя

    self.distanceMaster = cDistanceMaster:new(self.opponents) --управляет перемещением оппонентов

    self.result = utils.createText()    --отладочная строка с результатом боя
    
end

--[[
Обработчик действий.
ПАРАМЕТРЫ: action - действие, initiator - инициатор действия.
Пока isStackAlive true, идет отрезок боя. isStackAlive false означает конец отрезка.
Манипулируя isStackAlive, разрешаю/запрещаю писать в стек.
]]
function FightMaster:handleAction(action, stateType, initiator)
    -- print("handleAction", action, stateType, initiator)
    local time = os.clock() --время действия

    --Если на вход атака, то проверяю готовность стека
    if (action == Constants.text.stateAttack) then
        if not self.isStackAlive then
            --если стека нет, то разрешаю запись
            self.isStackAlive = true
            --действие, которое будет записано
            local item = { name = action, typeId = stateType, initiator = initiator, time = time} 
            --помечаю инициатора стека
            self.stackInitiator = initiator
            self.initTime = time
            --записываю действие в стек
            self.stack:push(item)
            self:addUpdate()
        else
            --если стек уже существует, то добавляю атаку в стек
            local item = { name = action, typeId = stateType, initiator = initiator, time = time}
            self.stack:push(item)
            -- self.other = initiator

            local DISABLE_ACTIONS_EVENT = { name="disable_actions" }
            Runtime:dispatchEvent( DISABLE_ACTIONS_EVENT )
            self.isStackAlive = false
            self:removeUpdate()
            self:reviewStack()
        end
    --Если на вход блок
    elseif (action == Constants.text.stateBlock) then
        print("Handle action stateBlock")
        --то если стек готов к записи
        if self.isStackAlive then
            local item = { name = action, typeId = stateType, initiator = initiator, time = time}
            --записываю действие в стек
            self.stack:push(item)
            -- self.other = initiator
        end
    ----------------------- ВЕТВЬ ПЕРЕСТАЛА РАБОТАТЬ в 0.1.6
    --Но если блок был отпущен, то на вход попадает покой от противника
    -- elseif (action == Constants.text.stateIdle and initiator ~= self.stackInitiator) then
        -- if self.isStackAlive then
            --и я извлекаю его: блок не защитан!
            -- self.stack:pop()
        -- end
    -----------------------
    --Если на вход попадает покой от инициатора стека, то это конец жизни стека!
    elseif (action == Constants.text.stateIdle and initiator == self.stackInitiator) then
        self.isStackAlive = false
        --начинаю разбор стека
        self:reviewStack()
        self:removeUpdate()
    elseif (action == Constants.text.statePowerfulAttack) then
        print("action == Constants.text.statePowerfulAttack", self.isStackAlive)
        local item = { name = action, typeId = stateType, initiator = initiator, time = time}
        self.stack:push(item)
        self.isStackAlive = false
        --в версии 0.1.7.5 оказалось, что нет необходимости вызывать разбор стека второй раз,
        --т.к. он вызовется по завершению основной атаки.
        --начинаю разбор стека
        -- self:reviewStack()
        self:removeUpdate()
    end
end

--[[
Здесь происходит разбор стека.
]]
function FightMaster:reviewStack()
    print("****reviewStack******")
    if self.stack:isEmpty() then
        print("stack is empty")
        return
    else

        self:getOther()

        --извлекаю действие
        local item = self.stack:pop()
        -- print("self.stack item:", item.name, item.stackInitiator, item.time)

        --Если действие - блок, тогда засчитать успешный блок.
        --Логика построена на следствии: если элемент в стеке равен блоку, то элементов 
        --стеке два, потому что на первом месте в стеке всегда атака.
        if item.name == Constants.text.stateBlock then
            local attackItem = self.stack:pop()
            local attackId = attackItem.typeId
            print("reviewStack : Constants.text.stateBlock")
            
            if attackId == item.typeId then
                self:showResult(Constants.text.sucsessfullBlock)
            else
                self.other:getDamage()
                self:showResult(Constants.text.sucsessfullAttack)
            end

        --но если действие - атака
        elseif (item.name == Constants.text.stateAttack) then
            --тогда проверяю: действительно ли это ответное действие (в этом случае в 
            --стеке после извлечения остался еще один элемент)
            if not self.stack:isEmpty() then
                --и если да, то засчитываю контратаку
                -- print("Counter-attack-time!", item.time)
                -- print("Init attack-time!", self.initTime)

                local initTimeAttack = self.initTime
                local initTimeCounterAttack = item.time
                -- print("Init time attack:", initTimeAttack)
                -- print("Init time counter-attack:", initTimeCounterAttack)

                local endTimeAttack = initTimeAttack + self.stackInitiator:getAttackTime()/1000
                local endTimeCounterAttack = initTimeCounterAttack + self.other:getAttackTime()/1000
                -- print("End time attack:", endTimeAttack)
                -- print("End time counter-attack:", endTimeCounterAttack)

                if endTimeAttack >= endTimeCounterAttack then
                    self:showResult(Constants.text.sucsessfullCounterattack)
                    self.stackInitiator:getDamage()
                else
                    self:showResult(Constants.text.unsucsessfullCounterattack)
                    self.other:getDamage()
                end

            -- но если стек пуст, значит атака была успешной
            else
                --тогда проверяю хитрый случай: что если блок был зажат до начала атаки?
                --тогда стек был еще не инициализирован, иенно поэтому сейчас он пуст
                local isOtherStateBlock = self.other:getState() == Constants.text.stateBlock

                local result
                if not isOtherStateBlock then
                    result = Constants.text.sucsessfullAttack
                    self.other:getDamage()
                else
                    result = Constants.text.sucsessfullBlock
                end                    

                self:showResult(result)
            end

        -- действие - мощная атака
        elseif (item.name == Constants.text.statePowerfulAttack) then
            print("item.name == Constants.text.statePowerfulAttack", item.initiator.id)

            local result
            local isOtherStateBlock = self.other:getState() == Constants.text.stateBlock
            if isOtherStateBlock then
                print("BLOCK")
                result = Constants.text.sucsessfullBlock
            else
                result = Constants.text.sucsessfullPowerfulAttack
                print("DAMAGE, THROW BACK, COOLDOWN OTHER")
            end

            self:showResult(result)
            -- print("***** IS_OTHER_STATE_BLOCK", isOtherStateBlock)
            -- self.distanceMaster:throwBack(self.other, self.stackInitiator)
            -- self.other:getDamage()
            -- self:showResult(Constants.text.sucsessfullPowerfulAttack)
            -- print("hp after powerful", self.other.currentHealthPoints)
        end

        --извлекаю оставшийся после разбора элемент, чтобы подготовить стек к новому 
        --отрезку боя
        while not self.stack:isEmpty() do
            self.stack:pop()
        end

    end
end

--[[
Добавляю update.
В нем вывожу в консоль значение self.isStackAlive чтобы видеть время жизни стека.
Пока идет вывод - разрешена запись в стек, идет отрезок боя.
]]
function FightMaster:addUpdate()
    self.update = function(event)
        -- print("isStackAlive", self.isStackAlive)
    end
    Runtime:addEventListener( "enterFrame", self.update )
end

function FightMaster:removeUpdate()
    Runtime:removeEventListener( "enterFrame", self.update )
end

--[[
Получаю информацию об участниках боя, чтобы в дальнейшем изменить показания
счетчика жизни.
ПАРАМЕТРЫ: <character> character, enemy
]]
function FightMaster:setOpponents(character, enemy)
    self.opponents[1] = character
    self.opponents[2] = enemy
end

--[[
Определяю кто из участников боя является вторым (НЕ инициатором отрезка боя).
]]
function FightMaster:getOther()
    self.other = self.opponents[1]
    if self.stackInitiator.id == 1 then
        self.other = self.opponents[2]
    end
end

function FightMaster:showResult(resultText)
    self.result.text = resultText
    self.result.alpha = 1
    transition.to(self.result, {alpha=0, time=1000})
end

return FightMaster