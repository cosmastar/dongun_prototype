-----------------------------------------------------------------------------------------
--[[  
Класс выносливости: логика и графическое отображение.
Атака и блок расходуют выносливость.
Ввыносливость восстаналивается со временем при бездействии.
2017
--]]
-----------------------------------------------------------------------------------------
local Object = require("src.classes.object")
local Stamina = Object:extend()
local Constants = require("cfg.constants")

local baseWidth = 60

function Stamina:initialize(character)
    self.className = "Stamina"
    self.character = character
    
    self.objGroup = display.newGroup()
    self.objEmpty = {}
    self.objCurrent = {}
    self.currentLevel = {}

    self.update = {}

    self:createVisual()
    self:addUpdate()
    self:setAttackHandler()
    
end

function Stamina:createVisual()

    self.objEmpty = display.newRect( 0, 0, baseWidth, 20)
    self.objEmpty:setFillColor( 1, 1, 1 )
    self.objEmpty.anchorX = 0
    self.objCurrent = display.newRect( 0, 0, baseWidth, 20)
    self.objCurrent:setFillColor( 0, 1, 0 )
    self.objCurrent.anchorX = 0

--     local options = 
--     {
--         text = "Attack!",
--         x = self.obj.x,
--         y = self.obj.y,
--         font = native.systemFont,
--         fontSize = 26
--     }
--     self.text = display.newText( options )
--     self.text:setFillColor( 1 )

    self.objGroup:insert(self.objEmpty)
    self.objGroup:insert(self.objCurrent)
    -- self.objGroup:insert(self.text)

--     self:setAlpha(0)
--     self:addTouchHandlers()

end

-- Уменьшение уровня выносливости при блокировании.
function Stamina:decrease()
    if self.objCurrent.width > 1 then
        self.currentLevel = self.currentLevel - Constants.stamina.block
        self.objCurrent.width = self.currentLevel
    end
end

-- Восстановление уровня выносливости при бездействии.
function Stamina:increase()
    if self.objCurrent.width < baseWidth then
        self.currentLevel = self.currentLevel + Constants.stamina.block
        self.objCurrent.width = self.currentLevel
    end
end

--Функция изменяет значение выносливости на указанное
-- [ЧИСЛО] number - значение, на которое нужно уменьшить текущее значение выносливости 
function Stamina:change(number)
    self.currentLevel = self.currentLevel - number
    if self.currentLevel < 0 then
        self.currentLevel = 0
    end
    self.objCurrent.width = self.currentLevel
end

function Stamina:addUpdate()

    local this = self
    self.update = function(event)
        if (self.character:getState() == Constants.text.stateBlock) then
            this:decrease()
        elseif (self.character:getState() == Constants.text.stateIdle) then
            self:increase()
        end
        self.currentLevel =  self.objCurrent.width
    end
    Runtime:addEventListener("enterFrame", self.update)

end

-- function Stamina:setAlpha(alpha)
--     self.objGroup.alpha = alpha
-- end

function Stamina:setAttackHandler()
    local this = self
    local START_ATTACK_EVENT = { name="start_attack", iniciator = nil }
    local attackHandler = function( event )
        if event.iniciator == self.character.id then
            this:change(baseWidth/3)
        end
    end
    Runtime:addEventListener( START_ATTACK_EVENT.name, attackHandler )
end

function Stamina:setPosition(x, y)
    self.objGroup.x = x - self.objGroup.width/2
	self.objGroup.y = y
end

function Stamina:destroy()
    Runtime:removeEventListener("enterFrame", self.update)
end

return Stamina